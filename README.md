ToscA plugin can be inserted into a DAW (Digital Audio Workstation, such as ProTools, Apple Logic, Digital Performer, Ableton Live, etc.) in order to send/receive parameters’ automation.

It is based on the OSC ([Open Sound Control](http://opensoundcontrol.org)) protocol.

The ToscA plugin does not process any audio (the audio signals are just bypassed actually).

The ToscA plugin is available in the following formats: AU, VST, VST3, AAX.
 
> **References**
>
> - [Thibaut Carpentier]( https://www.ircam.fr/person/thibaut-carpentier/), [ToscA : an OSC communication plugin for object-oriented spatialization authoring](http://opensoundcontrol.org). In Proc of 41st International Computer Music Conference (ICMC), Denton, TX, USA, pp 368 – 371, Sept 2015.
> - [Thibaut Carpentier]( https://www.ircam.fr/person/thibaut-carpentier/), [ToscA : un plugin de communication OSC pour le mixage spatialisé orienté objet](https://hal.archives-ouvertes.fr/hal-01247477v1). In Proc of Journées d’Informatique Musicale (JIM), Montréal, May 2015.
